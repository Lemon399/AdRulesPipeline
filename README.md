#### Ad Rules Sync Pipeline

此项目提供用于 [Bitbucket](https://support.atlassian.com/bitbucket-cloud/docs/bitbucket-pipelines-configuration-reference/) 和 [GitLab / 极狐](https://docs.gitlab.cn/jh/ci/yaml/index.html) 的两套流水线 YAML，用于批量下载给定 ABP 规则，然后批量同步到指定仓库，是 ABP 规则的搬运工 :)

当前将以下规则 :

- [AdRules](https://adrules.top/)
- [几十 KB 的轻量规则](https://github.com/damengzhu/banad)
- [ABP Merge Rules](https://github.com/damengzhu/abpmerge)
- [混合规则](https://lingeringsound.github.io/adblock/)
- [ADSLJ](https://gitlink.org.cn/zzp282/ads)

同步到以下仓库 :

- Bitbucket https://bitbucket.org/lemon399/ad-rules
- Codeberg https://codeberg.org/lemon399/AdRules
- Gitea https://gitea.com/lemon399/AdRules
- GitLab https://gitlab.com/lemon399/AdRules

2023-11-04 开始，每天两次，早晚十点各一次 (UTC+8)
2023-12-25 Bitbucket 流水线空跑导致本月时间用尽，现在只在晚十点同步一次，下月恢复

##### 前提条件

1. 操作仓库使用 SSH 密钥授权，请确定所有仓库账户使用相同公钥
2. 对于 Bitbucket ，需要将 **SSH 密钥对** 填写进
   Repository settings > SSH Keys 中，并且依次获取发布仓库的 Known hosts
3. 对于 Gitlab / 极狐，需要将使用 Ed25519 算法生成的 **SSH 密钥对文件** ( `id_ed25519` 和 `id_ed25519.pub` ) 上传至 设置 > CI/CD > 安全文件

##### 添加 ABP 规则

对于 bitbucket-pipelines.yml :

在首个 parallel 中添加一组 step ，修改必要信息即可，如果下载规则不需要使用 git 可使用 `image: alpine:latest`

对于 .gitlab-ci.yml :

添加一组 `stage: build` 的作业 ，修改必要信息即可

##### 添加发布仓库

> 如果发布仓库在 GitLab ，需要在 设置 > 仓库 > 受保护分支 中允许默认分支的 强制推送

对于 bitbucket-pipelines.yml :

在第二个 parallel 中添加一组 step ，修改必要信息即可

对于 .gitlab-ci.yml :

在 parallel 的 matrix 中添加一组变量 ：

```yaml
# 发布源 # 发布仓库主页
- raw_repo: 仓库文件直链地址，后面会接文件名
  git_repo: git 协议的 clone 地址
  gdbn_repo: 仓库默认分支名
  host_repo: 发布源域名，known_hosts 需要
  fpmx_repo: 发布源 SSH 指纹算法，known_hosts 需要
  fppk_repo: 发布源 SSH 指纹，known_hosts 需要
```
